package com.javarush.test.level24.lesson14.big01;

import java.util.ArrayList;

/**
 * Created by andrey.eliseev on 9/9/2016.
 */
public class Arcanoid
{
    private int width;
    private int height;
    private Ball ball;
    private Stand stand;
    private ArrayList<Brick> bricks;
    public static Arcanoid game;

    public Arcanoid(int width, int height)
    {
        this.width = width;
        this.height = height;
    }

    public void setBall(Ball ball)
    {
        this.ball = ball;
    }

    public void setStand(Stand stand)
    {
        this.stand = stand;
    }

    public void setBricks(ArrayList<Brick> bricks)
    {
        this.bricks = bricks;
    }

    public Ball getBall()
    {
        return ball;
    }

    public Stand getStand()
    {
        return stand;
    }

    public ArrayList<Brick> getBricks()
    {
        return bricks;
    }

    public int getWidth()
    {
        return width;
    }

    public int getHeight()
    {
        return height;
    }

    public void setWidth(int width)
    {
        this.width = width;
    }

    public void setHeight(int height)
    {
        this.height = height;
    }

    public void run() {

    }

    public void move() {

    }

    public static void main(String[] args)
    {
        game = new Arcanoid(100, 100);
        // TODO: 9/13/2016  A LOT OF THINGS 
        int width = game.getWidth();
        int height = game.getHeight();
        // FIXME: 9/13/2016 ANOTHER COMMENT TO DO LATER
    }
}
