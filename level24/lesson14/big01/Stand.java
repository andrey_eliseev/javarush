package com.javarush.test.level24.lesson14.big01;

/**
 * Created by andrey.eliseev on 9/9/2016.
 */
public class Stand extends BaseObject
{

    public Stand(double x, double y)
    {
        super(x, y, 2);
    }

    @Override
    public void move()
    {

    }

    @Override
    public void draw(Canvas canvas)
    {

    }
}
